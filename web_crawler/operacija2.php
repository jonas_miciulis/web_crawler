<?php
include("include/session.php");
global $bool;
global $ecounter;
if ($session->logged_in) {
	//Prisijungimas prie duomenu bazes
	$dbc = mysqli_connect('stud.if.ktu.lt', 'jonmic', 'kingeng6eeth2eiM', 'jonmic');
	if(!$dbc ){
		die('Negaliu prisijungti: '.mysqli_error($dbc));
	}
	if (mysqli_connect_errno()) {
		die('Connect failed: '.mysqli_connect_errno().' : '.
		mysqli_connect_error());
	}
	
	if(isset($_POST["ok"])) {
	
	//iconv() naudojamas, kad uzkoduoti UTF-8 raides i latin1 (toks nustatytas duomenu bazes)
	mysqli_query($dbc, "
		INSERT INTO skelbimai (marke, modelis, miestas, metai, telefonas, kaina, tekstas)
		VALUES (
			'".iconv("utf-8", "utf-8", $_POST["marke"])."',
			'".iconv("utf-8", "utf-8", $_POST["modelis"])."',
			'".iconv("utf-8", "utf-8", $_POST["miestas"])."',
			'".iconv("utf-8", "utf-8", $_POST["metai"])."',
			'".iconv("utf-8", "utf-8", $_POST["telefonas"])."',
			'".iconv("utf-8", "utf-8", $_POST["kaina"])."',
			'".iconv("utf-8", "utf-8", $_POST["tekstas"])."'
		)
	");

	$useriai = mysqli_query($dbc, "SELECT * FROM users");
	
	$bool = False;
	$ecounter = 0;
	
	while($useris = mysqli_fetch_assoc($useriai)):
		$level = $useris['userlevel'];
		$name = $useris['username'];
		$email = $useris['email'];
		if($level == USER_LEVEL) {
			if($mailer->sendEmail($name, $email)) {
				 $bool = True;
				 $ecounter++;
			}
		}
	endwhile;
	
}
	
	$irasai = mysqli_query($dbc, "SELECT * FROM skelbimai");
    ?>
    <html>
        <head>  
            <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8"/> 
            <title>Pridėkite savo skelbimą!</title>
            <link href="include/styles.css" rel="stylesheet" type="text/css" />
	<!--		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

			
        </head>
        <body>
            <table class="center" >
                <tr><td>
                        <img src="pictures/top.png"/>
                    </td></tr><tr><td> 
                        <?php
                        //Jei vartotojas prisijungęs
						include("include/meniu.php");
                        ?>
                        <table style="border-width: 2px; border-style: dotted;"><tr><td>
                                    Atgal į [<a href="index.php">Pradžia</a>]
                                </td></tr></table>               
                        <br> 
						<?php
						//cia reik dirbti
						?>
                        <div style="text-align: center;color:blue">                   
                            <h1>Skelbimų pridėjimas</h1>   
						(matomas tik darbuotojų ir administratoriaus)
                        </div> 
						<br>
						<br>
								<div class="container" style="text-align:left;">
								<form method='post'>
								<div class="form-group col-lg-6">
								<label for="marke" class="control-label">Automobilio markė:</label>
								<input name='marke' id='marke' type='text' class="form-control input-sm" required="true" >
								</div>
								<br>
								<div class="form-group col-lg-6">
								<label for="modelis" class="control-label">Automobilio modelis:</label>
								<input name='modelis' id="modelis" type='text' class="form-control input-sm" required="true">
								</div>
								<br>
								<div class="form-group col-lg-6">
								<label for="miestas" class="control-label">Miestas:</label>
								<input name='miestas' id='miestas' type='text' class="form-control input-sm" required="true">
								</div>
								<br>
								<div class="form-group col-lg-6">
								<label for="metai" class="control-label">Automobilio gamybos metai:</label>
								<input name='metai' id="metai"  type='text' class="form-control input-sm" required="true">
								</div>
								<br>
								<div class="form-group col-lg-6">
								<label for="telefonas" class="control-label">Telefonas:</label>
								<input name='telefonas' id="telefonas"  type='text' class="form-control input-sm" required="true">
								</div>
								<br>
								<div class="form-group col-lg-6">
								<label for="kaina" class="control-label">Kaina:</label>
								<input name='kaina' id="kaina" type='number' step='100' class="form-control input-sm" required="true">
								</div>
								<br>
								<div class="form-group col-lg-12">
								<label for="tekstas" class="control-label">Papildoma informacija:</label>
								<input class="form-control input-sm" name='tekstas' id="tekstas" type='text'>
								</div>
								<br>
								<div class="form-group">
								<input type='submit' name='ok' value='Įrašyti' class="btn btn-default"">
								<br>
								<br>
								<?php
								if($bool == True) {
									echo "<p><b><span style='color:#008000;'>Naujas skelbimas sėkmingai išsaugotas ir visiems klientams buvo išsiųsti automatiniai el. laiškai</span>";
									echo "<p><b><span style='color:#008000;'>Išsiųstų el. laiškų skaičius: $ecounter</span>";
								} ?>
								</div>
								</form>
								</div>
						
						
                        <br>  
                <tr><td>
                        <?php
                        include("include/footer.php");
                        ?>
                    </td></tr>      
            </table>
        </body>
    </html>
    <?php
    //Jei vartotojas neprisijungęs, užkraunamas pradinis puslapis  
} else {
    header("Location: index.php");
}
?>