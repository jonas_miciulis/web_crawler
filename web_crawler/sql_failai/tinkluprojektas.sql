-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2016 at 11:24 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tinkluprojektas`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_guests`
--

DROP TABLE IF EXISTS `active_guests`;
CREATE TABLE IF NOT EXISTS `active_guests` (
  `ip` varchar(15) NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `active_users`
--

DROP TABLE IF EXISTS `active_users`;
CREATE TABLE IF NOT EXISTS `active_users` (
  `username` varchar(30) NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `active_users`
--

INSERT INTO `active_users` (`username`, `timestamp`) VALUES
('Administratorius', 1476738858);

-- --------------------------------------------------------

--
-- Table structure for table `banned_users`
--

DROP TABLE IF EXISTS `banned_users`;
CREATE TABLE IF NOT EXISTS `banned_users` (
  `username` varchar(30) NOT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `skelbimai`
--

DROP TABLE IF EXISTS `skelbimai`;
CREATE TABLE IF NOT EXISTS `skelbimai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marke` varchar(35) NOT NULL,
  `modelis` varchar(35) NOT NULL,
  `miestas` varchar(35) NOT NULL,
  `metai` year(4) NOT NULL,
  `telefonas` varchar(35) NOT NULL,
  `kaina` double NOT NULL,
  `tekstas` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `skelbimai`
--

INSERT INTO `skelbimai` (`id`, `marke`, `modelis`, `miestas`, `metai`, `telefonas`, `kaina`, `tekstas`) VALUES
(1, 'Audi', 'A3', 'Kaunas', 2015, '862222661', 21500, '2.0 l., sedanas'),
(2, 'Volkswagen', 'Golf', 'Vilnius', 2013, '862222662', 10500, '1.6 l., universalas'),
(3, 'Opel', 'Astra', 'Klaipėda', 2012, '862222663', 6900, '1.4 l., hečbekas'),
(4, 'Audi', 'A2', 'Kaunas', 2001, '862222664', 2150, '1.4 l., hečbekas'),
(5, 'Audi', 'S5', 'Vilnius', 2007, '862222665', 17000, '4.2 l., kupė'),
(6, 'Honda', 'Accord', 'Kaunas', 2009, '862222668', 8350, '2.2 l., universalas'),
(7, 'Volvo', 'V50', 'Kaunas', 2004, '862222666', 3250, '2.0 l., universalas'),
(8, 'Volvo', 'V50', 'Šiauliai', 2006, '862222667', 3400, '2.0 l., universalas');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(30) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `userlevel` tinyint(1) UNSIGNED NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `userid`, `userlevel`, `email`, `timestamp`) VALUES
('Valdytojas', 'fe01ce2a7fbac8fafaed7c982a04e229', '7ed2b87b255a0348b61226bd7c2ed5b4', 5, 'demo@ktu.lt', 1330553708),
('Administratorius', 'fe01ce2a7fbac8fafaed7c982a04e229', 'cb8dd13295004d34cf4e2bbd4c277910', 9, 'demo@ktu.lt', 1476738858),
('Vartotojas', 'fe01ce2a7fbac8fafaed7c982a04e229', '9a47f4552955b91bcd8850d73b00e703', 1, 'demo@ktu.lt', 1330553730);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
