-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 18, 2016 at 11:22 AM
-- Server version: 1.0.26
-- PHP Version: 5.6.26-1~dotdeb+zts+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jonmic`
--

-- --------------------------------------------------------

--
-- Table structure for table `skelbimai`
--

CREATE TABLE IF NOT EXISTS `skelbimai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marke` varchar(35) NOT NULL,
  `modelis` varchar(35) NOT NULL,
  `miestas` varchar(35) NOT NULL,
  `metai` year(4) NOT NULL,
  `telefonas` varchar(35) NOT NULL,
  `kaina` double NOT NULL,
  `tekstas` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `skelbimai`
--

INSERT INTO `skelbimai` (`id`, `marke`, `modelis`, `miestas`, `metai`, `telefonas`, `kaina`, `tekstas`) VALUES
(1, 'Audi', 'A3', 'Kaunas', 2015, '862222661', 21500, '2.0 l., sedanas'),
(2, 'Volkswagen', 'Golf', 'Vilnius', 2013, '862222662', 10500, '1.6 l., universalas'),
(3, 'Opel', 'Astra', 'Klaipėda', 2012, '862222663', 6900, '1.4 l., hečbekas'),
(4, 'Audi', 'A2', 'Kaunas', 2001, '862222664', 2150, '1.4 l., hečbekas'),
(5, 'Audi', 'S5', 'Vilnius', 2007, '862222665', 17000, '4.2 l., kupė'),
(6, 'Honda', 'Accord', 'Kaunas', 2009, '862222668', 8350, '2.2 l., universalas'),
(7, 'Volvo', 'V50', 'Kaunas', 2004, '862222666', 3250, '2.0 l., universalas'),
(8, 'Volvo', 'V50', 'Šiauliai', 2006, '862222667', 3400, '2.0 l., universalas');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
